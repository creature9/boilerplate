const path = require("path");
const pkgDir = require("pkg-dir");

module.exports = {
  path: path.join(pkgDir.sync(), "dist", "client"),
  filename: "bundle.js"
};
