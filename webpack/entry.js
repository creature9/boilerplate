const path = require("path");
const pkgDir = require("pkg-dir");

module.exports = path.join(pkgDir.sync(), "src", "client", "ts", "index.tsx");
