const path = require("path");
const fs = require("fs");
const pkgDir = require("pkg-dir");

const rootDir = pkgDir.sync();
const packageJson = require(path.join(rootDir, "package.json"));

const config = require("./_config");

module.exports = `node ${path.join(config.SCRIPTS_DIR, "scripts", "index.js")}`;

let finalScripts = {};

// Get the list of commands.
const dirs = fs.readdirSync(path.join(rootDir, config.SCRIPTS_DIR));

dirs.forEach(dir => {
  let files = fs.readdirSync(path.join(rootDir, config.SCRIPTS_DIR, dir));
  files = files.filter(file => file.indexOf("_") !== 0);

  files.forEach(file => {
    let name = path.basename(file, path.extname(file));

    if (name === "index") {
      name = dir;
    } else {
      name = `    ${dir}:${name}`;
    }

    console.log(name);

    finalScripts[name.trim()] = require(path.join(
      rootDir,
      config.SCRIPTS_DIR,
      dir,
      file
    ));
  });
});

packageJson.scripts = finalScripts;
fs.writeFileSync(
  path.join(rootDir, "package.json"),
  JSON.stringify(packageJson, null, 2)
);
