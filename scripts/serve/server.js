const path = require("path");
const pkgDir = require("pkg-dir");

module.exports = `node ${path.join(
  pkgDir.sync(),
  "dist",
  "server",
  "index.js"
)}`;
