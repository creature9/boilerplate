# Build System

The build system is based off NPM scripts that are specified by a convention of 
folder names and file names.  Take a look in `scripts/` to see what's going on.

To both parse the directory structure and list the available scripts:

```
yarn scripts
```

# Running

```
yarn serve
```