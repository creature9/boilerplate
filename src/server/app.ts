import express from "express";

import { applyRoutes } from "./routes/applyRoutes";

const app = express();

applyRoutes(app);

export default app;
