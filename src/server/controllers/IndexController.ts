import { Request, Response } from "express";

export default class IndexController {
  index(req: Request, res: Response): void {
    res.json({
      mesage: "Hello World"
    });
  }
}
