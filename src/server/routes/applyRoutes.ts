import { Application } from "express";

// Controllers -----------------------------------------------------------------
import IndexController from "../controllers/IndexController";

export function applyRoutes(app: Application) {
  app.get("/", new IndexController().index);
}
