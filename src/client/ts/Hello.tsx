import * as React from "react";

export interface HelloProptypes {
  compiler: string;
  framework: string;
}

export class Hello extends React.Component<HelloProptypes, {}> {
  render() {
    return <h1>Hello World</h1>;
  }
}
