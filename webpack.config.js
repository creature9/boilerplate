module.exports = {
  entry: require("./webpack/entry"),
  output: require("./webpack/output"),
  devtool: require("./webpack/devtool"),
  resolve: require("./webpack/resolve"),
  module: require("./webpack/module/index"),
  externals: require("./webpack/externals")
};
